/**
 * Created by Dani on 08.03.2017.
 */
public class Athlete {

    public String name;
    public int startnummer;
    public double startzeit;

    public Athlete(String s, int i) {
    }

    public double getRadfahrenErgebnisse() {
        return radfahrenErgebnisse;
    }

    public void setRadfahrenErgebnisse(double radfahrenErgebnisse) {
        this.radfahrenErgebnisse = radfahrenErgebnisse;
    }

    public double radfahrenErgebnisse;


    public double getSchwimmenergebnisse() {
        return schwimmenergebnisse;
    }

    public void setSchwimmenergebnisse(double schwimmenergebnisse) {
        this.schwimmenergebnisse = schwimmenergebnisse;
    }

    public double schwimmenergebnisse;


    public double getLaufenErgbeniss() {
        return laufenErgbeniss;
    }

    public void setLaufenErgbeniss(double laufenErgbeniss) {
        this.laufenErgbeniss = laufenErgbeniss;
    }

    public double laufenErgbeniss;
}

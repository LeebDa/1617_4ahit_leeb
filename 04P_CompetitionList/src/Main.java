import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Dani on 08.03.2017.
 */
public class Main {

    public static void main(String[] args) throws IOException {

        double durchschnittLaeufer = 0;
        double durchschnittSchwimmer = 0;
        double durchschnittRadfahren = 0;
        List<String> athleten = new ArrayList<String>(Arrays.asList("Martin Solveig", "Timmy trumpet", "Christiano Ronaldo","Lionel Messi", "Chicharito", "Guido Burgstaller","Marc Janko",
               "Marko Arnautovic", "Peter Haslinger", "Robert Almer","Zlatan Ibrahimovic", "mark Andre Ter Stegen", "Andres Iniesta", "Pervao Pervan", "Marcel Hirscher", "Benjamin Raich",
                "DVBBS", "Darius und Finlay", "Andreas Gabalier", "Sido", "SpongeBOZZ"));


        List<Athlete> sportlerList = new ArrayList<>();
        for(int i = 0;i <sportlerList.size();i++)
        {

            sportlerList.add(new Athlete(athleten.get(i), i));
        }

        sportler s1 = new sportler();
        List<Athlete> laufenlist = s1.getLaufen();
        List<Athlete> radfahrenlist = s1.getRadfahren();
        List<Athlete> schwimmenlist = s1.getSchwimmen();
        s1.laufen();
        s1.setSchwimmen();
        s1.setRadfahren();
        s1.setErgebnisse();

        Collections.sort(laufenlist, new LaufenComparator());
        Collections.sort(radfahrenlist, new RadfahrenComparator());
        Collections.sort(schwimmenlist, new SchwimmenComparator());

        for(int i = 0;i < laufenlist.size();i++)
        {
            System.out.println("Die Liste der L�ufer" + laufenlist.get(i));
            durchschnittLaeufer = durchschnittLaeufer + laufenlist.get(i).startzeit;
        }
        for(int i = 0; i < radfahrenlist.size(); i++)
        {
            System.out.println("Die Liste der Radfahrer: " + radfahrenlist.get(i));
            durchschnittRadfahren = durchschnittRadfahren + laufenlist.get(i).startzeit;
        }
        for(int i = 0; i < schwimmenlist.size();i++)
        {
            System.out.println("Die Liste der Schwimmer" + schwimmenlist.get(i));
            durchschnittSchwimmer = durchschnittSchwimmer + laufenlist.get(i).startzeit;
        }


        System.out.println("Die beste  der L�ufer und damit die Bestzeit im Bewerb hat ist: " + laufenlist.get(0).name + "\n" + "Der Zweite ist:" + laufenlist.get(1).name + "\n" + "Der Dritte ist: " + laufenlist.get(2).name);
        System.out.println("Die beste  der Radfahrer ist und damit der Beste im Bewerb: " + radfahrenlist.get(0).name + "\n" + "Der Zweite ist:" + radfahrenlist.get(1).name + "\n" + "Der Dritte ist: " + radfahrenlist.get(2).name);
        System.out.println("Die beste  der Schwimmer und damit der beste im Bewerb ist: " + schwimmenlist.get(0).name + "\n" + "Der Zweite ist:" + schwimmenlist.get(1).name + "\n" + "Der Dritte ist: " + schwimmenlist.get(2).name);

        System.out.println("Die Besetzeit im Laufen: " + laufenlist.get(0).startzeit);
        System.out.println("Die Besezeit im Radfahren: " + radfahrenlist.get(0).startzeit);
        System.out.println("Die Bestzeit im Schwimmen: " + schwimmenlist.get(0).startzeit);

        durchschnittLaeufer = durchschnittLaeufer / 7;
        durchschnittRadfahren = durchschnittRadfahren / 7;
        durchschnittSchwimmer = durchschnittSchwimmer / 7;
        System.out.println("*---------------------------*");

        System.out.println("Die Durchschnittszeit im Laufen:" + durchschnittLaeufer);
        System.out.println("Die Durchschnittszeit im Radfahren: " + durchschnittRadfahren);
        System.out.println("Die Durchschnittszeit im Schwimmen" + durchschnittSchwimmer);


    }
}

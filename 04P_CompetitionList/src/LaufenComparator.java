import java.util.Comparator;

/**
 * Created by Dani on 22.03.2017.
 */
public class LaufenComparator implements Comparator<Athlete> {

    @Override
    public int compare(Athlete a1, Athlete a2)
    {
        if(a1.laufenErgbeniss > a2.laufenErgbeniss)
        {
            return 1;
        }
        else
        if(a1.laufenErgbeniss == a2.laufenErgbeniss)
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }
}

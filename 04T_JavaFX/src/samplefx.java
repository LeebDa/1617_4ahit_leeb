import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Created by Dani on 19.01.2017.
 */
public class samplefx  extends Application{


    @Override
    public void start(Stage primarystage)
    {
        primarystage.setTitle("JavaFX Welcome");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Scene scene = new Scene(grid, 300, 275);

        primarystage.setScene(scene);

        Button btn_Click = new Button("Test");
        grid.add(btn_Click,1,1);

        Label text = new Label("This is a test");
        grid.add(text, 2, 1);

        primarystage.show();
        btn_Click.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                text.setText("Now its done");
            }
        });



    }




}

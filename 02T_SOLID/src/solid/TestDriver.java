package solid;

import java.util.ArrayList;

/**
 * Created by Dani on 12.11.2015.
 */
public class TestDriver {
    public static void main(String[] args) {
        Manager m = new Manager();
        ArrayList<IWorker> workers = new ArrayList<>();
        Worker w1, w2, w3;
        w1 = new Worker();
        w2 = new Worker();
        w3 = new Worker();
        SuperWorker sw = new SuperWorker();

        workers.add(w1);
        workers.add(w2);
        workers.add(w3);
        workers.add(sw);
        m.setWorker(workers);
        m.manage();




    }
}

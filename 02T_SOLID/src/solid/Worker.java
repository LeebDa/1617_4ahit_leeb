package solid;

/**
 * Created by Dani on 05.11.2015.
 */
public class Worker implements IWorker{
    public void work() { //arbeiter arbeitet
// ...working
        System.out.println("The Worker is now working!");
    }
    public void eat() { //arbeiter macht Pause
// ...eating in launch break
        System.out.println("The worker is now eating!");
    }
}

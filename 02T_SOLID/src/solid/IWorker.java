package solid;

/**
 * Created by Dani on 05.11.2015.
 */
public interface IWorker {
    void work();
}

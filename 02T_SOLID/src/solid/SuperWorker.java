package solid;

/**
 * Created by Dani on 05.11.2015.
 */
class SuperWorker  implements IWorker{
    public void work() { //Ein arbeiter f�ngt zum arbeiten an
// ...working much more
        System.out.println("The best worker is now working !");
    }
    public void eat() // Ein arbeiter macht Pause und ist etwas
    {
        System.out.println("The SuperWorker is eating!");
    }
}


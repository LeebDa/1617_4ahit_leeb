package solid;

/**
 * Created by Dani on 05.11.2015.
 */
class Robot implements IWorker{
    public void work() { //Robot arbeitet
// ....working
        System.out.println("The robot is now working");
    }
}
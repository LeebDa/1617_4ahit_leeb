/**
 * Created by Dani on 19.10.2016.
 */
class BottleBox <T extends Drink> {

    private Object[] bottles = new Object[6];
    private int count = 0;

    public boolean isFull() {
        /*if(bottles.length <= 6) {
        return true;
    }
        return false;
    */
        return false;
    }

    public int getBottleCount() { return bottles.length; }

    public int getCapacity() { return count; }

    public void add(Bottle<T> bottle) {
        if (this.isFull())
            throw new IllegalStateException();
        this.bottles[this.count] = bottle;
        this.count++;
    }

    public Bottle<T> getBottle(int index) {
        return (Bottle<T>) this.bottles[index];
    }
}
/**
 * Created by Dani on 19.10.2016.
 */
public class TDGenericDrinks {
    public static void main(String[] args) {

        Bottle<Beer> beerBottle = new Bottle<Beer>();
        Bottle<Beer> beerBottle2 = new Bottle<Beer>();
        Bottle<Wine> wineBottle = new Bottle<Wine>();

        beerBottle.fill(new Beer("Eggenberger"));
        System.out.println("Is Beer Empty?" + beerBottle.isEmpty());

        System.out.println("We will drink beer!");

        //System.out.println("\n \n");

        beerBottle.empty();
        System.out.println("Is Beer Empty?" + beerBottle.isEmpty());

        BottleBox<Beer> beerBottleBox = new BottleBox<Beer>();

        beerBottleBox.add(beerBottle);

        beerBottleBox.add(beerBottle2);

        beerBottle2.fill(new Beer("Eggenberger"));

        System.out.println("BeerbottleBox = " + beerBottleBox.getBottle(0));
        System.out.println("Beer" + beerBottleBox.getBottle(1));


        /*for(int i = 0; i <= beerBottleBox.getBottleCount(); i++)
        {
            //beerBottle.fill(new Beer("Bier"));
            beerBottleBox.add(new Bottle<Beer>());
        }
        System.out.println("BeerBottleBox Nummer 1" + beerBottleBox.getBottle(1));
*/

    }
}

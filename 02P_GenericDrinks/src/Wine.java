/**
 * Created by Dani on 12.10.2016.
 */
abstract class Wine extends Drink {
    private final String region;

    public Wine(String region) {
        this.region = region;
    }

    public String getRegion() {
        return this.region;
    }

    public String toString() {return null;}
}
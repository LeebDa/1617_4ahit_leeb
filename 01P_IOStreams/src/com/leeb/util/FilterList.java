package com.leeb.util;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Dani on 12.10.2016.
 */
public class FilterList {


    public static List<String> FilterList (String file) throws Exception
    {
        List<String> content = new ArrayList<>();
        content = Files.lines(Paths.get(file)).map(s -> s.trim().replaceAll("\\s+",""))
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toList());

        return content;
    }

}

package com.leeb.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
 * Created by Dani on 05.10.2016.
 */

/**
 *
 */
public class FilleUtils {
    /**
     *
     * @param fileName
     * @return
     */
    public static String readTextFile(String fileName)
    {
        try {
            String content = new String(Files.readAllBytes(Paths.get(fileName)));
            return content;
        } catch (IOException e) {
            e.printStackTrace();

        }
        return null;
    }

    /**
     *
     * @param filename
     * @return
     */
    public static List<String> readTextFilebyLine (String filename)
    {
        try {
            List<String> lines = Files.readAllLines(Paths.get(filename));
            return lines;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param filename
     * @param content
     */
    public static void writeToTextFile(String filename, String content)
    {
        try {
            Files.write(Paths.get(filename), content.getBytes(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.leeb.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by Dani on 05.10.2016.
 */
public class Test_Driver {
    public static void main(String[] args) throws IOException {

        //Testing the readTextFile Method
       String filename1 = "C:\\Users\\Dani\\Documents\\4AHIT_2\\SEW\\SEW_Java\\4ahit_leeb\\01P_IOStreams\\test.txt";


        //Calling the Method to read all Lines from a File
        ReadAllLines(filename1);

        // Calling the Method to read line by line from a File
        ReadByLines(filename1);

        //Calling the method to write a file
        WriteToFile();
    }

    public static void ReadAllLines(String filename1)
    {
        String content1 = FilleUtils.readTextFile(filename1);
        System.out.println(content1);

    }

    public static void ReadByLines(String filename1)
    {
        List<String> content = FilleUtils.readTextFilebyLine(filename1);

        for(String s : content)
        {
            System.out.println(s);
        }

    }

    public static void WriteToFile() throws IOException {
        String writing = "Das ist ein Versuch um in eine Datei zu schreiben";
        FileWriter fw = new FileWriter("C:\\Users\\Dani\\Documents\\4AHIT_2\\SEW\\SEW_Java\\4ahit_leeb\\01P_IOStreams\\test2.txt");
        BufferedWriter bf = new BufferedWriter(fw);
        bf.write(writing);
        bf.newLine();

        bf.close();

    }
}
